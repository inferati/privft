import codecs
import spacy


def convert_to_character_level(fpath):
  s = spacy.load('en')
  with codecs.open(fpath + '.char', 'w', encoding='utf8') as wf:
    with codecs.open(fpath, 'r', encoding='utf8') as rf:
      for line in rf:
        parts = line.split('\t')
        tokens = [tok.text for tok in s.tokenizer(parts[0].strip())]
        label = parts[1]
        characters = ' |s| '.join([' '.join(t) for t in tokens])
        wf.write(characters)
        wf.write('\t')
        wf.write(label)


convert_to_character_level('.data/imdb/train.txt')
convert_to_character_level('.data/imdb/test.txt')
