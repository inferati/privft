import os
import glob
import io
from torchtext import data

DATASET_URL = 'http://inferati.blob.core.windows.net/public/research/privft/'


# adapted from https://github.com/pytorch/text/blob/master/torchtext/datasets/imdb.py
class CustomData(data.Dataset):
  @staticmethod
  def sort_key(ex):
    return len(ex.text)

  def __init__(self, path, text_field, label_field, **kwargs):
    """Create a custom dataset instance given a path and fields.
    Arguments:
      path: Path to the dataset's highest level directory
      text_field: The field that will be used for text data.
      label_field: The field that will be used for label data.
      Remaining keyword arguments: Passed to the constructor of
        data.Dataset.
    """
    fields = [('text', text_field), ('label', label_field)]
    examples = []

    with io.open(path + '.txt', 'r', encoding="utf-8") as f:
      for text in f:
        vals = text.split('\t')
        label = vals[1].replace('__label__', '')
        ex = data.Example.fromlist([vals[0], label], fields)
        examples.append(ex)
    super(CustomData, self).__init__(examples, fields, **kwargs)

  @classmethod
  def splits(cls, text_field, label_field, root='.data',
            train='train', test='test', **kwargs):
    """Create dataset objects for splits of the custom dataset.
    Arguments:
      text_field: The field that will be used for the sentence.
      label_field: The field that will be used for label data.
      dataset: Name of the dataset to download.
      root: Root dataset storage directory.
      train: The directory that contains the training examples
      test: The directory that contains the test examples
      Remaining keyword arguments: Passed to the splits method of
        Dataset.
    """
    return super(CustomData, cls).splits(
      root=root, text_field=text_field, label_field=label_field, 
      train=train, validation=None, test=test, **kwargs)

  @classmethod
  def iters(cls, batch_size=32, device=0, root='.data', vectors=None, **kwargs):
    """Create iterator objects for splits of the custom dataset.
    Arguments:
      batch_size: Batch_size
      device: Device to create batches on. Use - 1 for CPU and None for
        the currently active GPU device.
      root: The root directory that contains the custom dataset subdirectory
      vectors: one of the available pretrained vectors or a list with each
        element one of the available pretrained vectors (see Vocab.load_vectors)
      Remaining keyword arguments: Passed to the splits method.
    """
    TEXT = data.Field()
    LABEL = data.Field(sequential=False)

    train, test = cls.splits(TEXT, LABEL, root=root, **kwargs)

    TEXT.build_vocab(train, vectors=vectors)
    LABEL.build_vocab(train)

    return data.BucketIterator.splits(
      (train, test), batch_size=batch_size, device=device)


class Enron(CustomData):
  urls = [ DATASET_URL + 'enron.zip' ]
  name = 'enron'
  dirname = ''


class Youtube(CustomData):
  urls = [ DATASET_URL + 'youtube.zip' ]
  name = 'youtube'
  dirname = ''


class Yelp(CustomData):
  urls = [ DATASET_URL + 'yelp.zip' ]
  name = 'yelp'
  dirname = ''


class Imdb(CustomData):
  urls = [ DATASET_URL + 'imdb.zip' ]
  name = 'imdb'
  dirname = ''


class DBPedia(CustomData):
  urls = [ DATASET_URL + 'dbpedia.zip' ]
  name = 'dbpedia'
  dirname = ''


class AGNews(CustomData):
  urls = [ DATASET_URL + 'agnews.zip' ]
  name = 'agnews'
  dirname = ''