import torch
from torchtext import data
from customdata import Imdb, Yelp, DBPedia, AGNews, Enron, Youtube
from customfield import CustomField, CustomBrField, custom_preprocess
from torchtext import datasets
import random
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import time
import argparse
import sys
import math
from tqdm import tqdm
import numpy as np
import os


class FastText(nn.Module):
  def __init__(self, vocab_size, embedding_dim, output_dim, pad_idx, unk_idx):
    super().__init__()
    self.embedding = nn.Embedding(vocab_size, embedding_dim, padding_idx=pad_idx)
    self.fc = nn.Linear(embedding_dim, output_dim)
    self.out_dim = output_dim
    self.pad_idx = pad_idx
    self.unk_idx = unk_idx
      
  def forward(self, text):
    embedded = self.embedding(text)
    embedded = embedded.permute(1, 0, 2)
    pooled = F.avg_pool2d(embedded, (embedded.shape[1], 1)).squeeze(1) 
    return self.fc(pooled)


def count_parameters(model):
  return sum(p.numel() for p in model.parameters() if p.requires_grad)


def accuracy(preds, y, binary = True):
  """
  Compute accuracy (note that batch size is always 1).
  """
  if binary:
    #round predictions to the closest integer
    rounded_preds = torch.round(torch.sigmoid(preds))
    correct = (rounded_preds == y).float() #convert into float for division 
  else:
    _, i = torch.max(preds, 1)
    correct = (i == y).float()
  
  acc = correct.sum() / len(correct)
  return acc


def train(model, iterator, optimizer, criterion):
  epoch_loss = 0
  epoch_acc = 0
  model.train()
  for batch in iterator:
    optimizer.zero_grad()
    predictions = model(batch.text).squeeze(1)
    loss = criterion(predictions, batch.label)
    acc = accuracy(predictions, batch.label, model.out_dim == 1)
    loss.backward()
    optimizer.step()
    epoch_loss += loss.item()
    epoch_acc += acc.item()
  return epoch_loss / len(iterator), epoch_acc / len(iterator)


def evaluate(model, iterator, criterion):
  epoch_loss = 0
  epoch_acc = 0
  model.eval()
  with torch.no_grad():
    for batch in iterator:
      predictions = model(batch.text).squeeze(1)
      loss = criterion(predictions, batch.label)
      acc = accuracy(predictions, batch.label, model.out_dim == 1)
      epoch_loss += loss.item()
      epoch_acc += acc.item()
  return epoch_loss / len(iterator), epoch_acc / len(iterator)


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs


def main(arguments):
  global args
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('--save', type=str, default=None, help='name of the model to save')
  parser.add_argument('--dataset', type=str, default='imdb', help='name of dataset to download')
  parser.add_argument('--vocab', type=int, default=500000, help='number of epochs')
  parser.add_argument('--epoch', type=int, default=5, help='number of epochs')
  parser.add_argument('--pretrained', type=str, default='glove.6B.100d', help='pretrained embeddings')
  parser.add_argument('--emb', type=int, default=50, help='embedding vector size')
  parser.add_argument('--seed', type=int, default=1234, help='random seed value')
  parser.add_argument('--test', action='store_true', help='test-mode only, load specified args.save model')
  parser.add_argument('--dump', action='store_true', help='dump model and dataset into c++ consummable format')
  args = parser.parse_args(arguments)
  print(args)
  
  torch.manual_seed(args.seed)
  torch.cuda.manual_seed(args.seed)
  torch.backends.cudnn.deterministic = True

  TEXT = CustomField()
  LABEL = data.LabelField(dtype=torch.float)

  OUTPUT_DIM = 1
  if args.save is None:
    args.save = args.dataset + '.pt'
  if args.dataset == 'enron':
    TEXT = CustomBrField()
    train_data, test_data = Enron.splits(TEXT, LABEL)
  elif args.dataset == 'youtube':
    TEXT = CustomBrField()
    train_data, test_data = Youtube.splits(TEXT, LABEL)
  elif args.dataset == 'yelp':
    TEXT = CustomBrField()
    train_data, test_data = Yelp.splits(TEXT, LABEL)
  elif args.dataset == 'imdb':
    TEXT = CustomBrField()
    train_data, test_data = Imdb.splits(TEXT, LABEL)
  elif args.dataset == 'dbpedia':
    OUTPUT_DIM = 14
    LABEL = data.LabelField(dtype=torch.long)
    train_data, test_data = DBPedia.splits(TEXT, LABEL)
  elif args.dataset == 'agnews':
    OUTPUT_DIM = 4
    LABEL = data.LabelField(dtype=torch.long)
    train_data, test_data = AGNews.splits(TEXT, LABEL)
  else:
    print('Unrecognized dataset')
    sys.exit(0)
  
  train_data, valid_data = train_data.split(random_state=random.seed(args.seed))

  allowed_vectors = [
    'charngram.100d', 'fasttext.en.300d', 'fasttext.simple.300d', 
    'glove.42B.300d', 'glove.840B.300d', 'glove.twitter.27B.25d', 
    'glove.twitter.27B.50d', 'glove.twitter.27B.100d', 'glove.twitter.27B.200d', 
    'glove.6B.50d', 'glove.6B.100d', 'glove.6B.200d', 'glove.6B.300d'
  ]
  if args.pretrained in allowed_vectors:
    TEXT.build_vocab(train_data, vectors=args.pretrained, max_size=args.vocab)
  else:
    print('INFORMATION: Not using pretrained vectors.')
    TEXT.build_vocab(train_data, max_size=args.vocab)
  LABEL.build_vocab(train_data)

  device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
  train_iterator, valid_iterator, test_iterator = data.BucketIterator.splits(
      (train_data, valid_data, test_data), 
      batch_size=1, 
      device=device)

  INPUT_DIM = len(TEXT.vocab)
  PAD_IDX = TEXT.vocab.stoi[TEXT.pad_token]
  UNK_IDX = TEXT.vocab.stoi[TEXT.unk_token]

  model = FastText(INPUT_DIM, args.emb, OUTPUT_DIM, PAD_IDX, UNK_IDX)

  print(f'The model has {count_parameters(model):,} trainable parameters')

  # copy pretrained embeddings
  if args.pretrained in allowed_vectors:
    model.embedding.weight.data.copy_(TEXT.vocab.vectors)

  # zero out <unk> and <pad> token embeddings
  model.embedding.weight.data[UNK_IDX] = torch.zeros(args.emb)
  model.embedding.weight.data[PAD_IDX] = torch.zeros(args.emb)

  optimizer = optim.Adam(model.parameters())
  criterion = nn.BCEWithLogitsLoss() if OUTPUT_DIM == 1 else nn.CrossEntropyLoss()
  model = model.to(device)
  criterion = criterion.to(device)
  best_valid_acc = 0

  if args.test or args.dump:
    model.load_state_dict(torch.load(args.save))
  
  if args.test:
    test_loss, test_acc = evaluate(model, test_iterator, criterion)
    print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc*100:.2f}%')
    return

  if args.dump:
    embed = model.embedding.weight.data.cpu().numpy()
    np.savetxt(args.save + '.embed.cpp',
      embed,
      delimiter=' ', 
      fmt='%.10f', 
      header='{} {}'.format(embed.shape[0], embed.shape[1]),
      comments='')

    fc = model.fc.weight.data.cpu().numpy()
    bias = model.fc.bias.data.cpu().numpy()
    np.savetxt(args.save + '.fc.cpp',
      fc,
      delimiter=' ', 
      fmt='%.10f', 
      header='{} {}'.format(fc.shape[0], fc.shape[1]),
      footer=' '.join([str(f) for f in bias]),
      comments='')

    dataset_file = args.save + '.data.cpp'
    if os.path.exists(dataset_file):
      os.remove(dataset_file)

    with open(dataset_file, 'ab') as df:
      for batch in tqdm(test_iterator):
        np.savetxt(df, batch.label.cpu().numpy().astype(int), delimiter=' ', fmt='%i', newline = ' ')
        ds = np.transpose(batch.text.cpu().numpy().astype(int))
        np.savetxt(df, ds, delimiter=' ', fmt='%i')
    return

  for epoch in range(args.epoch):
    start_time = time.time()
    train_loss, train_acc = train(model, train_iterator, optimizer, criterion)
    valid_loss, valid_acc = evaluate(model, valid_iterator, criterion)
    end_time = time.time()

    epoch_mins, epoch_secs = epoch_time(start_time, end_time)
    if valid_acc > best_valid_acc:
        best_valid_acc = valid_acc
        torch.save(model.state_dict(), args.save)
    
    print(f'Epoch: {epoch+1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
    print(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc*100:.2f}%')
    print(f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc*100:.2f}%')

  model.load_state_dict(torch.load(args.save))
  test_loss, test_acc = evaluate(model, test_iterator, criterion)
  print(f'Test Loss: {test_loss:.3f} | Test Acc: {test_acc*100:.2f}%')


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
