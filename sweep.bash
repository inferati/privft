#!/bin/bash

trap "kill 0" SIGINT # allow to kill all spawning processes in the same group

if [ $1 == "-h" ]; then
  echo "Syntax::sh scriptname output-file gpu-id num-gpu"
  exit
fi

# check outputfile
if [ -z $1 ]; then
  echo "Output file not specified. Using 'privft.console' "
  OUTFILE="privft.console"
else
  OUTFILE=$1
  echo "Output File = " $OUTFILE
fi
if [ -e $OUTFILE ]; then
  echo "Output file " $OUTFILE " already exists, quitting."
  exit
fi

if [ -z $2 ]; then
  echo "number of gpus not specified, using 0"
  gpu=0
else
  gpu=$2
fi
save="privft_$gpu.pt"

if [ -z $3 ]; then
  echo "number of gpus not specified, using 1"
  numgpu=1
else
  numgpu=$3
fi

gpuid=0
dataset=("imdb")
configs=("--pretrained glove.6B.50d --emb 50" "--pretrained glove.6B.100d --emb 100" 
  "--pretrained glove.6B.200d --emb 200" "--pretrained glove.6B.300d --emb 300"
  "--pretrained glove.twitter.27B.200d --emb 200" "--pretrained glove.twitter.27B.100d --emb 100"
  "--pretrained glove.twitter.27B.50d --emb 50" "--pretrained glove.twitter.27B.25d --emb 25"
  "--pretrained glove.840B.300d --emb 300" "--pretrained glove.42B.300d --emb 300"
  "--pretrained fasttext.simple.300d --emb 300" "--pretrained fasttext.en.300d --emb 300")
for ds in "${dataset[@]}"; do
  for conf in "${configs[@]}"; do
    gpuid=$(((gpuid + 1) % numgpu))
    if [ "$gpuid" = "$gpu" ]; then
      printf "CUDA_VISIBLE_DEVICES=$gpuid python main.py --dataset $ds $conf \n" >> $OUTFILE 2>&1
      CUDA_VISIBLE_DEVICES=$gpuid python main.py --dataset $ds $conf >> $OUTFILE 2>&1
    fi
  done
done

echo "Done"
