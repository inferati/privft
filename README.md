# Prerequisites
- Python 3.7
- Pytorch (preferrably with GPU support): https://pytorch.org
- Run `pip install -r requirements.txt`

# Instructions
To train/test a model on some dataset, for example on imdb, simply run: `main.py --dataset imdb`. 

Valid options for dataset are `[imdb,yelp,dbpedia,agnews,enron,youtube]`. The code will automatically download the necessary data files. See `main.py` for complete parameter options.