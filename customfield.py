import torch
from torchtext import data


def custom_preprocess(x):
  bigrams = set(zip(*[x[i:] for i in range(2)]))
  trigrams = set(zip(*[x[i:] for i in range(3)]))
  
  for n_gram in bigrams:
    x.append(' '.join(n_gram))
  for n_gram in trigrams:
    x.append(' '.join(n_gram))
  return x


class CustomField(data.Field):
  def __init__(self, preprocess=custom_preprocess, *args, **kwargs):
    super().__init__(tokenize='spacy', lower=True, preprocessing=preprocess, *args, **kwargs)


class CustomBrField(CustomField):
  def __init__(self, preprocess=custom_preprocess, *args, **kwargs):
    super().__init__(preprocess=preprocess, *args, **kwargs)

  def preprocess(self, x):
    x = x.replace('<br />', '')
    return super().preprocess(x)

